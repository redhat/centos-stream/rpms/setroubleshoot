#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/setroubleshoot/Regression/embedded-null-byte-in-audit-records
#   Description: Is sealert able to processes audit messages which contain embedded null bytes?
#   Author: Milos Malik <mmalik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="setroubleshoot"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm ${PACKAGE}
        rlRun "rpm -qf /usr/sbin/ausearch"
        rlRun "rpm -qf /usr/bin/audit2allow"
        rlRun "rpm -qf /usr/bin/sealert"
        OUTPUT_FILE=`mktemp`
    rlPhaseEnd

    rlPhaseStartTest "bz#1775135 + bz#1776199"
        if rlIsRHEL 7 ; then
            rlRun "ausearch -i -m avc -if ./audit.log"
            rlRun "audit2allow -i ./audit.log"
            # if sealert got stuck, kill it after 15 seconds
            rlWatchdog "sealert -a ./audit.log 2>&1 | tee ${OUTPUT_FILE}" 15
        else # Fedora, RHEL-8 and above
            rlRun "ausearch -i -m avc -if ./short.log"
            rlRun "audit2allow -i ./short.log"
            # if sealert got stuck, kill it after 15 seconds
            rlWatchdog "sealert -a ./short.log 2>&1 | tee ${OUTPUT_FILE}" 15
        fi
        rlRun "grep -i -e Traceback -e TypeError -e embedded -e \"null byte\" ${OUTPUT_FILE}" 1
        rlRun "grep -i \"Plugin catchall\" ${OUTPUT_FILE}" 0
        if [ $? -ne 0 ]; then cat ${OUTPUT_FILE}; fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rm -f ${OUTPUT_FILE}
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

