#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/setroubleshoot/Regression/no-plugin-exception-during-analyses
#   Description: Does setroubleshoot report any 'Plugin Exception' during analyses?
#   Author: Petr Lautrbach <plautrba@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2020 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="setroubleshoot"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm ${PACKAGE}-server
        rlAssertRpm ${PACKAGE}-plugins
    rlPhaseEnd

    rlPhaseStartTest "no 'Plugin Exception'"
        SINCE=$(date '+%Y-%m-%d %H:%M:%S')
        RANDOM_NUMBER=${RANDOM}
        rlRun "passwd --help >& /root/output-${RANDOM_NUMBER}.txt"
        rlRun "rm -f /root/output-${RANDOM_NUMBER}.txt"
        sleep 10
        rlRun "journalctl --since=\"$SINCE\" > journal-after.txt"
        STATUS=0
        rlAssertGrep "setroubleshoot.*: SELinux is preventing (/usr/bin/)?passwd" journal-after.txt -E
        [[ $? -eq 0 ]] || STATUS=$?
        rlAssertNotGrep "setroubleshoot.*: Plugin Exception " journal-after.txt
        [[ $? -eq 0 ]] || STATUS=$?
        rlRun "[[ $STATUS -eq 0 ]] || cat journal-after.txt"
        rlRun "rm -f journal-after.txt"
    rlPhaseEnd

    rlPhaseStartTest "no 'Plugin Exception' in short.log"
        OUTPUT_FILE=`mktemp`
        rlRun "sealert -a ./short.log >& $OUTPUT_FILE"
        STATUS=0
        rlAssertNotGrep "'generator' object is not subscriptable" $OUTPUT_FILE
        [[ $? -eq 0 ]] || STATUS=$?
        rlAssertGrep "Plugin catchall_labels" $OUTPUT_FILE
        [[ $? -eq 0 ]] || STATUS=$?
        rlRun "[[ $STATUS -eq 0 ]] || cat $OUTPUT_FILE"
        rlRun "rm -f $OUTPUT_FILE"
    rlPhaseEnd

    rlPhaseStartCleanup
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

